<?php

namespace Database\Factories;

use App\Models\Categoria;
use App\Models\Post;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Post>
 */
class PostFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Post::class;
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'Categorias_id' => $this->faker->unique()->numberBetween(1, Categoria::count()),
            'titulo' => $this->faker->sentence(),
            'contenido' => $this->faker->text(),
            'fecha_creacion' => $this->faker->dateTime(),
            'fecha_actualizacion' => $this->faker->dateTime()
        ];
    }
}
