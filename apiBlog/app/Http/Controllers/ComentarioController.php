<?php

namespace App\Http\Controllers;

use App\Models\Comentario;
use App\Models\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Exception;
use Carbon\Carbon;

class ComentarioController extends Controller
{

    /**
     * Agrega un comentario a la base da tos
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request, Comentario $comentario)
    {
        try {
            $validator = Validator::make($request->all(), [
                'Posts_id' => 'required',
                'contenido' => 'required',
            ], [
                'required' => 'El campo :attribute es requerido'
            ]);

            if ($validator->fails()) {
                return jsend_fail($validator->errors());
            }

            //Validamos si el post existe
            $PostsExiste = Post::where('id', '=', $request->Posts_id)->get();
            if (count($PostsExiste) === 0) {
                return jsend_fail("El post con el id: " . $request->Posts_id . ", no existe", 404);
            }


            $fecha_creacion = Carbon::now()->toDateTimeString();

            $comentario->Posts_id = $request->Posts_id;
            $comentario->contenido = $request->contenido;
            $comentario->fecha_creacion = $fecha_creacion;
            $comentario->fecha_actualizacion = $fecha_creacion;

            if ($comentario->save()) {
                return jsend_success($comentario, status: 200);
            }
        } catch (Exception $e) {
            return jsend_error($e);
        }
    }

    /**
     * Actualiza un comentario
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id, Comentario $comentario)
    {
        try {
            $validator = Validator::make($request->all(), [
                'Posts_id' => 'required',
                'contenido' => 'required',
            ], [
                'required' => 'El campo :attribute es requerido'
            ]);

            if ($validator->fails()) {
                return jsend_fail($validator->errors());
            }

            //Validamos si el id del comentario existe
            $comentarioPorId = Comentario::where('id', '=',  $id)->get();
            if (count($comentarioPorId) === 0) {
                return jsend_fail("El comentario con el id: " . $id . ", no existe", 404);
            }

            //Validamos si el post existe
            $postExiste = Post::where('id', '=',  $request->Posts_id)->get();
            if (count($postExiste) === 0) {
                return jsend_fail("El post con el id: " . $request->Posts_id . ", no existe", 404);
            }

            //Setemoa la fecha de creacion del recurso
            foreach ($comentarioPorId as $key => $value) {
                $fecha_creacion = $value['fecha_creacion'];
            }

            $fecha_actualizacion = Carbon::now()->toDateTimeString();

            $comentario = Comentario::find($id);
            $comentario->Posts_id = $request->Posts_id;
            $comentario->contenido = $request->contenido;
            $comentario->fecha_creacion = $fecha_creacion;
            $comentario->fecha_actualizacion = $fecha_actualizacion;

            if ($comentario->update()) {
                return jsend_success($comentario, status: 200);
            }
        } catch (Exception $e) {
            return jsend_error($e);
        }
    }

    /**
     * Elimina un comentario
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        try {
            //Si el comentario se elimina retorn true y nos da success, Si no dira que el comentario no existe
            $comentarioDelete = Comentario::where('id', $id)->delete();
            if (!$comentarioDelete) {
                return jsend_fail("El comentario con el id: " . $id . ", no existe", 404);
            }

            return jsend_success("Comentario con el id: " . $id . ", ha sido eliminado");
        } catch (Exception $e) {
            return jsend_error("Hay un registro vinculado a este comentario, no se puede eliminar");
        }
    }


    /**
     * Muestra un comentario por id
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {

            $listaComentario = Comentario::where('id', '=', $id)->get();
            //Validamos si la lista de comentarios tiene mas de un dato, Si no es por que el id no existe
            if (count($listaComentario) === 0) {
                return jsend_fail("El comentario con el id: " . $id . ", no existe", 404);
            }

            return jsend_success($listaComentario, 200);
        } catch (Exception $e) {
            return jsend_error("Hay un registro vinculado a este comentario, no se puede eliminar");
        }
    }

    /**
     * Muestra un comentario por id
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function ver($id)
    {
        try {

            $listaComentario = Comentario::where('id', '=', $id)->get();
            //Validamos si la lista de comentarios tiene mas de un dato, Si no es por que el id no existe
            if (count($listaComentario) === 0) {
                return jsend_fail("El comentario con el id: " . $id . ", no existe", 404);
            }

            return jsend_success($listaComentario, 200);
        } catch (Exception $e) {
            return jsend_error("Hay un registro vinculado a este comentario, no se puede eliminar");
        }
    }

    /**
     * Muestra todos los comentarios
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function verTodo()
    {
        try {

            $listaComentario = Comentario::all();

            return jsend_success($listaComentario, 200);
        } catch (Exception $e) {
            return jsend_error($e);
        }
    }
}
