<?php

namespace App\Http\Controllers;

use App\Models\Categoria;
use App\Models\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;
use Exception;


class PostController extends Controller
{
    /**
     * Agrega un nuevo Post a la base de datos
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request, Post $post)
    {
        try {
            $validator = Validator::make($request->all(), [
                'titulo' => 'required',
                'contenido' => 'required',
                "Categorias_id" => 'required',
            ], [
                'required' => 'El campo :attribute es requerido'
            ]);

            if ($validator->fails()) {
                return jsend_fail($validator->errors());
            }
            //Validamos si la categoria existe
            $categoriaEnPosts = Categoria::where('id', '=', $request->Categorias_id)->get();
            if (count($categoriaEnPosts) === 0) {
                return jsend_fail("La categoria con el id: " . $request->Categorias_id . ", no existe", 404);
            }

            $fecha_creacion = Carbon::now()->toDateTimeString();

            $post->titulo = $request->titulo;
            $post->contenido = $request->contenido;
            $post->Categorias_id = $request->Categorias_id;
            $post->fecha_creacion = $fecha_creacion;
            $post->fecha_actualizacion = $fecha_creacion;


            if ($post->save()) {
                return jsend_success($post, status: 200);
            }
        } catch (Exception $e) {
            return jsend_error($e);
        }
    }

    /**
     * Actualiza Un Posts
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id, Post $post)
    {
        try {
            $validator = Validator::make($request->all(), [
                'titulo' => 'required',
                'contenido' => 'required',
                "Categorias_id" => 'required',
            ], [
                'required' => 'El campo :attribute es requerido'
            ]);

            if ($validator->fails()) {
                return jsend_fail($validator->errors());
            }


            $postPorId = Post::where('id', '=', $id)->get();
            foreach ($postPorId as $key => $value) {
                $fecha_creacion = $value['fecha_creacion'];
            }
            //Validamos si el id del post existe
            if (count($postPorId) === 0) {
                return jsend_fail("El post con el id: " . $id . ", no existe", 404);
            }
            //Validamos si la categoria existe
            $categoriaExiste = Categoria::where('id', '=', $request->Categorias_id)->get();
            if (count($categoriaExiste) === 0) {
                return jsend_fail("La categoria con el id: " . $request->Categorias_id . ", no existe", 404);
            }

            $fecha_actualizacion = Carbon::now()->toDateTimeString();

            $post = Post::find($id);
            $post->titulo = $request->titulo;
            $post->contenido = $request->contenido;
            $post->Categorias_id = $request->Categorias_id;
            $post->fecha_creacion = $fecha_creacion;
            $post->fecha_actualizacion = $fecha_actualizacion;

            if ($post->update()) {
                return jsend_success($post, status: 200);
            }
        } catch (Exception $e) {
            return jsend_error("El recurso no existe");
        }
    }

    /**
     * Elimina un post
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        try {
            $postDelete = Post::where('id', $id)->delete();
            //validamos si el post se elimina, retorna false o true
            if (!$postDelete) {
                return jsend_fail("El post con el id: " . $id . ", no existe", 404);
            }

            return jsend_success("Post con el id: " . $id . ", ha sido eliminado");
        } catch (Exception $e) {
            return jsend_error("Hay un registro vinculado a este post, no se puede eliminar");
        }
    }

    /**
     * Muestra un post por id
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {

            $postPorId = Post::where('id', '=', $id)->get();
            //Validamos si el id existe
            if (count($postPorId) === 0) {
                return jsend_fail("El post con el id: " . $id . ", no existe", 404);
            }

            return jsend_success($postPorId, 200);
        } catch (Exception $e) {
            return jsend_error("Hay un registro vinculado a este post, no se puede eliminar");
        }
    }

    /**
     * Muestra un post por id
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function ver($id)
    {
        try {

            $postPorId = Post::where('id', '=', $id)->get();
            //Validamos si el id existe
            if (count($postPorId) === 0) {
                return jsend_fail("El post con el id: " . $id . ", no existe", 404);
            }

            return jsend_success($postPorId, 200);
        } catch (Exception $e) {
            return jsend_error("Hay un registro vinculado a este post, no se puede eliminar");
        }
    }

    /**
     * Muestra todos los post
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function verTodo()
    {
        try {

            $listaPost = Post::all();
            return jsend_success($listaPost, 200);
        } catch (Exception $e) {
            return jsend_error($e);
        }
    }
}
