<?php

namespace App\Http\Controllers;

use App\Models\Categoria;
use Carbon\Carbon;
use Exception;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;

class CategoriaController extends Controller
{

    /**
     * Agrega una categoria a la base de datos
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request, Categoria $categoria)
    {
        try {
            $validator = Validator::make($request->all(), [
                'nombre' => 'required',
            ], [
                'required' => 'El campo :attribute es requerido'
            ]);

            //Retorna los errores de validacion
            if ($validator->fails()) {
                return jsend_fail($validator->errors());
            }

            $fecha_creacion = Carbon::now()->toDateTimeString();
            $categoria->nombre = $request->nombre;
            $categoria->fecha_creacion = $fecha_creacion;
            $categoria->fecha_actualizacion = $fecha_creacion;

            if ($categoria->save()) {
                return jsend_success($categoria, status: 200);
            }
        } catch (Exception $e) {
            return jsend_error($e);
        }
    }

    /**
     * Actualiza una categoria en la base de datos
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id, Categoria $categoria)
    {
        try {
            $validator = Validator::make($request->all(), [
                'nombre' => 'required',
            ], [
                'required' => 'El campo :attribute es requerido'
            ]);

            if ($validator->fails()) {
                return jsend_fail($validator->errors());
            }
            //Nos traemos la fecha de creacion del recurso
            $categoriaPorId = Categoria::where('id', '=', $id)->get();
            foreach ($categoriaPorId as $key => $value) {
                $fecha_creacion = $value['fecha_creacion'];
            }
            //Validamos si la categoria existe
            if (count($categoriaPorId) === 0) {
                return jsend_fail("La categoria con el id: " . $id . ", no existe", 404);
            }

            $fecha_actualizacion = Carbon::now()->toDateTimeString();

            $categoria = Categoria::find($id);
            $categoria->nombre = $request->nombre;
            $categoria->fecha_creacion = $fecha_creacion;
            $categoria->fecha_actualizacion = $fecha_actualizacion;

            if ($categoria->update()) {
                return jsend_success($categoria, status: 200);
            }
        } catch (Exception $e) {
            return jsend_error($e);
        }
    }

    /**
     * Elimina una categoria
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        try {
            $categoriaDelete = Categoria::where('id', $id)->delete();
            //Si le categoria no elimina es por que no existe, en caso de que tenga un registro entra al catch
            if (!$categoriaDelete) {
                return jsend_fail("La categoria con el id: " . $id . ", no existe", 404);
            }

            return jsend_success("Categoria con el id: " . $id . ",ha sido eliminado");
        } catch (Exception $e) {
            return jsend_error("Hay un registro vinculado a esta categoria, no se puede eliminar");
        }
    }
    /**
     * Muestra una categoria por id
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {

            $listaCategoria = Categoria::where('id', '=', $id)->get();
            //Validamos si el id de la categoria existe
            if (count($listaCategoria) === 0) {
                return jsend_fail("La categoria con el id: " . $id . ", no existe", 404);
            }

            return jsend_success($listaCategoria, 200);
        } catch (Exception $e) {
            return jsend_error("Hay un registro vinculado a esta categoria, no se puede eliminar");
        }
    }

    /**
     * Muestra una categoria por id
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function ver($id)
    {
        try {

            $listaCategoria = Categoria::where('id', '=', $id)->get();
            //Validamos si el id de la categoria existe
            if (count($listaCategoria) === 0) {
                return jsend_fail("La categoria con el id: " . $id . ", no existe", 404);
            }

            return jsend_success($listaCategoria, 200);
        } catch (Exception $e) {
            return jsend_error($e);
        }
    }

    /**
     * Muestra todas las categorias
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function verTodo()
    {
        try {

            $listaCategoria = Categoria::all();
            return jsend_success($listaCategoria, 200);
        } catch (Exception $e) {
            return jsend_error($e);
        }
    }
}
